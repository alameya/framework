package text

import (
	"fmt"
	"regexp"
	"strings"
)

var stripTagsRegexp,
	addRelNoFollowAttrMainRegexp,
	addRelNoFollowAttrSubRegexp,
	fetchExternalResourcesRegexp *regexp.Regexp

const noFollowAttr = "nofollow noreferrer"
const relNoFollow = `rel="nofollow noreferrer"`
const whiteSpaceRune = ' '

func init() {
	stripTagsRegexp = regexp.MustCompile("<[^>]+>?")
	addRelNoFollowAttrMainRegexp = regexp.MustCompile(`<a\s+[^>]+?>`)
	addRelNoFollowAttrSubRegexp = regexp.MustCompile(`rel=["'](?P<value>[^"']+)["']`)
	fetchExternalResourcesRegexp = regexp.MustCompile(`src=["'](?P<value>http[^"']+)["']`)
}

func StripTags(text string) string {
	text = stripTagsRegexp.ReplaceAllString(text, " ")
	text = doubledWhiteSpaces.ReplaceAllString(text, ` `)
	text = strings.TrimSpace(text)

	return text
}

// TODO implement recursive removing
func RemoveTag(htmlIn string, tags ...string) string {
	for _, tag := range tags {
		pattern := fmt.Sprintf("<%[1]s>.+?</%[1]s>", tag)
		htmlIn = regexp.MustCompile(pattern).ReplaceAllString(htmlIn, "")
	}

	return htmlIn
}

func AddRelNoFollowAttr(htmlIn string) string {
	replacer := func(subMatch string) string {
		pockets := addRelNoFollowAttrSubRegexp.FindStringSubmatch(subMatch)
		if len(pockets) == 0 {
			subMatchRunes := []rune(subMatch)
			relNoFollowRunes := []rune(relNoFollow)
			resultRunes := append([]rune{}, subMatchRunes[:3]...)
			resultRunes = append(resultRunes, relNoFollowRunes...)
			resultRunes = append(resultRunes, whiteSpaceRune)
			resultRunes = append(resultRunes, subMatchRunes[3:]...)
			resultString := string(resultRunes)
			return resultString
		} else if len(pockets) == 2 && pockets[1] != noFollowAttr {
			return addRelNoFollowAttrSubRegexp.ReplaceAllString(subMatch, relNoFollow)
		}

		return subMatch
	}

	return addRelNoFollowAttrMainRegexp.ReplaceAllStringFunc(htmlIn, replacer)
}

func FetchExternalResources(htmlIn string) (resources []string) {
	matches := fetchExternalResourcesRegexp.FindAllStringSubmatch(htmlIn, -1)
	for _, match := range matches {
		fmt.Println(match)
		resources = append(resources, match[1])
	}

	return
}