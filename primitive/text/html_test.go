package text

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestStripTags(t *testing.T) {
	testTable := map[string]string{
		`<title>thoas/go-funk: A modern Go utility library which provides helpers (map, find, contains, filter, ...)</title>
		<meta name="description" content="A modern Go utility library which provides helpers (map, find, contains, filter, ...) - thoas/go-funk">
		<link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
		<link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
		<meta property="fb:app_id" content="1401488693436528">`: `thoas/go-funk: A modern Go utility library which provides helpers (map, find, contains, filter, ...)`,

		` <meta name="viewport" content="width=device-width">
  <title>thoas/go-funk: A modern Go utility library which provides helpers (map, find, contains, filter, ...)</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
  <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
  <meta property="fb:app_id" content="14014886`: `thoas/go-funk: A modern Go utility library which provides helpers (map, find, contains, filter, ...)`,
		`<div class="select-menu-no-results">Nothing to show</div>`: `Nothing to show`,
		`  </td>
          <td class="content">
            <span class="css-truncate css-truncate-target"><a class="js-navigation-open" title="" id="ac389a623f099fc727fc1f3edca6853f-0bd463725bde59e75331b6cdf65d818b2be905c2" href="/thoas/go-funk/blob/master/compact_test.go">compact_test.go</a></span>
          </td>
          <td class="message">
            <span class="css-truncate css-truncate-target">
                  <a data-pjax="true" title="Implement funk.Compact" class="message" href="/thoas/go-funk/commit/9955deea9e463187dfcc0dd8a1ed95661c28f775">Implement funk.Compact</a>
            </span>
          </td>
          <td class="age">`: `compact_test.go Implement funk.Compact`,
	}

	for textIn, expectation := range testTable {
		Convey("TestStripTags", t, func() {
			testCaseStripTags(textIn, expectation)
		})

	}
}

func testCaseStripTags(textIn, expectation string) {
	testText := StripTags(textIn)
	Convey(testText, func() {
		So(testText, ShouldEqual, expectation)
	})
}

func TestRemoveTag(t *testing.T) {
	HtmlIn := `<some-tag>123<pre><code>some <b>text</b></code></pre></some-tag>`
	htmlOut := RemoveTag(HtmlIn, "pre")
	Convey("TestRemoveTag", t, func() {
		So(htmlOut, ShouldEqual, `<some-tag>123</some-tag>`)
	})
}

type TestCaseAddAttrToTag struct {
	htmlIn       string
	htmlExpected string
}

func TestAddAttrToTag(t *testing.T) {
	testTable := [...]TestCaseAddAttrToTag{
		{
			htmlIn:       `<p>sometext<img src="/icon.png"/><a href="https://github.com/fluidicon.png" title="GitHub"></a></p>`,
			htmlExpected: `<p>sometext<img src="/icon.png"/><a rel="nofollow noreferrer" href="https://github.com/fluidicon.png" title="GitHub"></a></p>`,
		},
		{
			htmlIn:       `<p>sometext<a rel="nofollow" href="https://github.com/fluidicon.png" title="GitHub"></a></p>`,
			htmlExpected: `<p>sometext<a rel="nofollow noreferrer" href="https://github.com/fluidicon.png" title="GitHub"></a></p>`,
		},
		{
			htmlIn:       `<a rel="nofollow noreferrer" href="https://github.com/fluidicon.png" title="GitHub"></a>`,
			htmlExpected: `<a rel="nofollow noreferrer" href="https://github.com/fluidicon.png" title="GitHub"></a>`,
		},
	}

	for _, testCase := range testTable {
		htmlOut := AddRelNoFollowAttr(testCase.htmlIn)
		Convey("TestStripTags", t, func() {
			So(htmlOut, ShouldEqual, testCase.htmlExpected)
		})
	}
}

func TestFetchExternalResources(t *testing.T) {
	testTable := [...]struct {
		htmlIn string
		links  []string
	}{
		{
			htmlIn: `<p>sometext<img src="https://github.com/fluidicon.png"/><a href="https://github.com/fluidicon2.png" title="GitHub"></a></p>`,
			links: []string{
				`https://github.com/fluidicon.png`,
			},
		},
		{
			htmlIn: `<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127821402-1"></script><link rel="stylesheet" href="https://github.com/style.css"><img src="https://github.com/fluidicon.png"/>`,
			links: []string{
				`https://www.googletagmanager.com/gtag/js?id=UA-127821402-1`,
				`https://github.com/fluidicon.png`,
			},
		},
	}

	for _, testCase := range testTable {
		Convey("TestFetchExternalResources", t, func() {
			links := FetchExternalResources(testCase.htmlIn)
			for i, link := range links {
				So(link, ShouldEqual, testCase.links[i])
			}

		})
	}
}
