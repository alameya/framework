package metric

import "fmt"

const gtmTpl = `<script async src="https://www.googletagmanager.com/gtag/js?id=%[1]s"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '%[1]s');
    </script>`

func BuildGoogleTagManager(uid string) string {
	return fmt.Sprintf(gtmTpl, uid)
}