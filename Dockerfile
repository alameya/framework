FROM golang:1.11.1-alpine3.8

RUN apk update && apk upgrade
RUN apk add openssh
RUN cat /dev/zero | ssh-keygen -q -N ""
RUN apk add bash
RUN apk add git
RUN apk add gcc
RUN apk add musl-dev
RUN apk add zip
RUN echo 'Host *' >> /root/.ssh/config
RUN echo '  Hostname gitlab.com' >> /root/.ssh/config
