package yandex

import (
	"encoding/json"
	"errors"
	"fmt"
)

type Sitemap  struct {
	SitemapID      string   `json:"sitemap_id"`
	SitemapURL     string   `json:"sitemap_url"`
	LastAccessDate string   `json:"last_access_date"`
	ErrorsCount    int      `json:"errors_count"`
	UrlsCount      int      `json:"urls_count"`
	ChildrenCount  int      `json:"children_count"`
	Sources        []string `json:"sources"`
	SitemapType    string   `json:"sitemap_type"`
}

type Sitemaps struct {
	Sitemaps []Sitemap `json:"sitemaps"`
}

func (c *Client) Sitemaps(userID int64, hostID string) ([]Sitemap, error) {
	requestURL := fmt.Sprintf("/user/%d/hosts/%s/sitemaps/", userID, hostID)
	body, code, err := c.do("GET", requestURL, nil)
	if code != 200 {
		return nil, fmt.Errorf("status code is %d", code)
	}

	var resp Sitemaps

	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}

	return resp.Sitemaps, nil
}

func (c *Client) DeleteSitemap (userID int64, hostID, sitemapID string) error {
	requestURL := fmt.Sprintf("/user/%d/hosts/%s/user-added-sitemaps/%s/", userID, hostID, sitemapID)
	body, code, err := c.do("DELETE", requestURL, nil)
	if err != nil{
		return err
	}

	if code != 204 {
		var resp map[string]string
		err = json.Unmarshal(body, &resp)
		if err != nil {
			return  err
		}

		return fmt.Errorf("status code is %d", resp["error_message"])
	}

	return nil
}


func (c *Client) AddSitemap (userID int64, hostID, sitemapURL string) (string, error) {
	body := []byte(`{"url": "` + sitemapURL + `"}`)
	requestURL := fmt.Sprintf("/user/%d/hosts/%s/user-added-sitemaps/", userID, hostID)
	body, code, err := c.do("POST", requestURL, body)
	if err != nil{
		return "", err
	}


	var resp map[string]string
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return "", err
	}


	if code != 201 {
		if _, ok := resp["sitemap_id"]; ok {
			return resp["sitemap_id"], errors.New(resp["error_message"])
		}
		return "", errors.New(resp["error_message"])
	}

	return resp["sitemap_id"], nil
}