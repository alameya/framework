package yandex

import (
	"encoding/json"
	"fmt"
)

func (c *Client) UserID() (int64, error) {
	body, code, err := c.do("GET", "/user", nil)
	if err != nil {
		return 0, err
	}

	if code != 200 {
		return 0, fmt.Errorf("status code is %d", code)
	}

	type Resp struct {
		UserID int64 `json:"user_id"`
	}

	var resp Resp

	err = json.Unmarshal(body, &resp)
	if err != nil {
		return 0, err
	}

	return resp.UserID, nil
}
