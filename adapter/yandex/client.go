package yandex

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

const apiUrlPrefix = "https://api.webmaster.yandex.net/v4"

type Client struct {
	oauth string
}

func NewClient(oauth string) *Client {
	return &Client{
		oauth: oauth,
	}
}

func (c *Client) do(method, relativeURI string, body []byte) ([]byte, int, error) {
	r, err := http.NewRequest(method, apiUrlPrefix + relativeURI, bytes.NewBuffer(body))
	if err != nil {
		return  nil, 0, err
	}

	r.Header.Set("Authorization", "OAuth " + c.oauth)
	r.Header.Set("Content-type", "application/json;charset=UTF-8")
	client := &http.Client{}

	resp, err := client.Do(r)
	if err != nil {
		return  nil, 0, err
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	return b, resp.StatusCode, nil
}


