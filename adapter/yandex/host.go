package yandex

import (
	"encoding/json"
	"errors"
	"fmt"
)

type Host struct {
	HostID         string `json:"host_id"`
	ASCIIHostURL   string `json:"ascii_host_url"`
	UnicodeHostURL string `json:"unicode_host_url"`
	Verified       bool   `json:"verified"`
	MainMirror     struct {
		HostID         string `json:"host_id"`
		Verified       bool   `json:"verified"`
		ASCIIHostURL   string `json:"ascii_host_url"`
		UnicodeHostURL string `json:"unicode_host_url"`
	} `json:"main_mirror"`
}

type HostsResponse struct {
	Hosts []Host `json:"hosts"`
}

func (c *Client) Hosts(userID int64) ([]Host, error) {
	body, code, err := c.do("GET", fmt.Sprintf("/user/%d/hosts/", userID), nil)
	if err != nil {
		return nil, err
	}

	if code != 200 {
		return nil, fmt.Errorf("status code is %d", code)
	}

	var resp HostsResponse

	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}

	return resp.Hosts, nil
}

func (c *Client) HostByURL(userID int64, siteURL string) (*Host, error) {
	hosts, err := c.Hosts(userID)
	if err != nil {
		return nil, err
	}

	for _, host := range hosts {
		if host.ASCIIHostURL == siteURL+"/" {
			return &host, nil
		}
	}

	return nil, fmt.Errorf("site %q is not found", siteURL)
}

func (c *Client) AddHost(userID int64, siteURL string) (string, error) {
	body := []byte(`{"host_url": "` + siteURL + `"}`)

	body, code, err := c.do("POST", fmt.Sprintf("/user/%d/hosts/", userID), body)
	if err != nil {
		return "", err
	}

	type Resp struct {
		HostID string `json:"host_id"`
	}

	var resp map[string]string

	err = json.Unmarshal(body, &resp)
	if err != nil {
		return "", fmt.Errorf("%s %v", body, err)
	}

	switch code {
	case 201:
		return resp["host_id"], nil
	case 403:
		return "", errors.New(resp["error_code"])
	case 409:
		return resp["host_id"], errors.New(resp["error_code"])
	default:
		return "", fmt.Errorf("unexpected response code %d", code)
	}
}
