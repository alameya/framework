package yandex

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

const (
	VERIFICATION_TYPE_DNS = "DNS"
	VERIFICATION_TYPE_HTML_FILE = "HTML_FILE"
	VERIFICATION_TYPE_META_TAG = "META_TAG"
	VERIFICATION_TYPE_WHOIS = "WHOIS"
)

type VerificationCodeResponse struct {
	VerificationUin        string `json:"verification_uin"`
	VerificationState      string `json:"verification_state"`
	VerificationType       string `json:"verification_type"`
	LatestVerificationTime string `json:"latest_verification_time"`
	FailInfo               *struct {
		Reason  string `json:"reason"`
		Message string `json:"message"`
	} `json:"fail_info"`
	ApplicableVerifiers []string `json:"applicable_verifiers"`
}

func (c *Client) verificationDo(method, url string) (*VerificationCodeResponse, int,  error) {
	body, code, err := c.do(method, url, nil)
	if err != nil {
		return nil, 0, err
	}

	var resp VerificationCodeResponse
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, 0, fmt.Errorf("%s %v", body, err)
	}

	return &resp, code, nil
}

func (c *Client) VerificationCode(userID int64, hostID string) (*VerificationCodeResponse, int, error) {
	requestURL := fmt.Sprintf("/user/%d/hosts/%s/verification/", userID, hostID)

	return c.verificationDo("GET", requestURL)
}

func (c *Client)Verification(userID int64, hostID string, method string) (*VerificationCodeResponse, int, error) {
	requestURL := fmt.Sprintf("/user/%d/hosts/%s/verification/?verification_type=%s", userID, hostID, method)

	return c.verificationDo("POST", requestURL)
}


func WriteVerificationPage(publicPath, code string) error {
	body := []byte(`<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>Verification: `+ code +`</body>
	</html>`)

	path := strings.TrimRight(publicPath, "/") + "/" + code + ".html"

	return ioutil.WriteFile(path, body, 0664)
}