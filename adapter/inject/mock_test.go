package inject

type MockFirst struct {
	Title string
}


type MockSecond struct {
	Title      string
	Dependency *MockFirst `inject`
}