package native

import (
	"errors"
	"gitlab.com/alameya/framework/adapter/messaging/pubsub"
	"sync"
)

const ErrorEventNotRegisteredMessage = "event is not registered message"

type consumerFunc = func(payload []byte) error

type subscriptions map[string][]consumerFunc

type PubSub struct {
	subscriptions subscriptions
	mx            sync.RWMutex
}

func NewPubSub() pubsub.Instance {
	return &PubSub{
		subscriptions: make(subscriptions),
		mx:            sync.RWMutex{},
	}
}

func (bus *PubSub) Subscribe(topic pubsub.Topic, consumer consumerFunc) error {
	bus.mx.Lock()
	if _, exists := bus.subscriptions[topic]; !exists {
		bus.subscriptions[topic] = []consumerFunc{}
	}

	bus.subscriptions[topic] = append(bus.subscriptions[topic], consumer)
	bus.mx.Unlock()

	return nil
}

func (bus *PubSub) Fire(event pubsub.Event) (err error) {
	bus.mx.RLock()
	if _, exists := bus.subscriptions[event.Topic()]; !exists {
		return errors.New(ErrorEventNotRegisteredMessage)
	}

	for _, consumer := range bus.subscriptions[event.Topic()] {
		err = consumer(event.Payload())
	}

	bus.mx.RUnlock()
	return err
}

type Event struct {
	pubsub.Event
	name    string
	payload []byte
}

func NewEvent(name string, payload []byte) pubsub.Event {
	return &Event{
		name:    name,
		payload: payload,
	}
}

func (event *Event) Topic() pubsub.Topic {
	return event.name
}

func (event *Event) Payload() []byte {
	return event.payload
}
