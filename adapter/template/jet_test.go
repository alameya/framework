package template

import (
	"bytes"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/alameya/framework/adapter/log"
	"strings"
	"testing"
)

type mockJetViewModel struct {
	Value string
}

func TestJetTemplate(t *testing.T) {
	var writer bytes.Buffer
	var data mockJetViewModel

	globalFunctions := make(map[string]interface{})
	globalFunctions["liftmeup"] = func(str string) string {
		return strings.ToUpper(str)
	}

	data.Value = "value"
	logObj := new(log.Log)
	logObj.AddProviders(log.NewMockLogger)
	tpl := NewJetTemplate(".", false, globalFunctions)
	tpl.MustCompile("test.jet").Render(&writer, data)

	Convey("Render2", t, func() {
		So(tpl.GetError(), ShouldBeNil)
		So(writer.String(), ShouldEqual, "test VALUE")
	})
}
