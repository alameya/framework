package template

import (
	"io"
)

/*
Template - is wrapper above template engines.
Object should not have been created directly
Use special constructor functions for that.
You should not to check error immediately
Usage:
tpl := New***Template(...)
tpl.MustCompile(...)
tpl.Render(...)
... or ...
New***Template(...).MustCompile(...).Render(...)

Cache for must compile should be implemented in provider
 */
type Template struct {
	Tpl
	err      error
	provider ProviderInterface
}

type Tpl interface {
	MustCompile(tplRelativePath string) Tpl
	Render(writer io.Writer, viewModel interface{}) Tpl
	GetError() error
	resetError()
	MustRender(tplRelativePath string, writer io.Writer, viewModel interface{})
}


type ProviderInterface interface {
	MustCompile(tplRelativePath string) error
	Render(writer io.Writer, viewModel interface{}) error
}

func (tpl *Template) MustCompile(tplRelativePath string) Tpl  {
	return tpl.do(func() error {
		return tpl.provider.MustCompile(tplRelativePath)
	})
}

func (tpl *Template) Render(writer io.Writer, viewModel interface{}) Tpl  {
	return tpl.do(func() error {
		return tpl.provider.Render(writer, viewModel)
	})
}

func (tpl *Template) GetError() error {
	return tpl.err
}

func(tpl *Template) MustRender(tplRelativePath string, writer io.Writer, viewModel interface{}) {
	if err := tpl.MustCompile(tplRelativePath).Render(writer, viewModel).GetError(); err!=nil {
		panic(err)
	}
}

func (tpl *Template) resetError() {
	tpl.err = nil
}

func (tpl *Template) do(action func() error) Tpl {
	if tpl.err != nil {
		return tpl
	}

	err := action()

	if err != nil {
		tpl.err = err
	}

	return tpl
}

