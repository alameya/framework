package template

import (
	"github.com/CloudyKit/jet"
	"io"
)

type jetTemplate struct {
	loader  *jet.Set
	compiledTpl *jet.Template
	ProviderInterface
}

func NewJetTemplate(path string, devMode bool, globalFunctions map[string]interface{}) Tpl {
	loader :=  jet.NewHTMLSet(path)
	loader = loader.SetDevelopmentMode(devMode)
	for key, globalFunction := range globalFunctions {
		loader.AddGlobal(key, globalFunction)
	}

	return &Template{
		provider: &jetTemplate{loader:loader},
	}
}

func (jetTpl *jetTemplate) MustCompile(tplRelativePath string) (err error) {
	jetTpl.compiledTpl, err = jetTpl.loader.GetTemplate(tplRelativePath)

	return
}

func (jetTpl *jetTemplate) Render(writer io.Writer, viewModel interface{}) error {
	return jetTpl.compiledTpl.Execute(writer, nil, viewModel)
}
