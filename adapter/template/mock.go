package template

import (
	"errors"
	"fmt"
	"io"
)

const (
	mockTplCorrectTestPrefix           = "%s:"
	mockTplCorrectTestAdditionalPart   = "%s"
	mockTplIncorrectTestPrefix         = ""
	mockTplIncorrectTestAdditionalPart = ""
)

type mockTemplate struct {
	prefix      string
	compiledTpl string
	ProviderInterface
}

type mockViewModel struct {
	prefix string
	text   string
}

func NewMockTemplate(prefix string) Tpl {
	var err error
	if prefix == "" {
		err = errors.New("prefix is empty")
	}

	return &Template{
		provider: &mockTemplate{prefix: prefix},
		err:      err,
	}
}

func (tpl *mockTemplate) MustCompile(additionalPart string) (err error) {
	tpl.compiledTpl = tpl.prefix + additionalPart

	if additionalPart == "" {
		err = errors.New("additionalPart can not be empty")
	}

	return
}

func (tpl *mockTemplate) Render(writer io.Writer, viewModel interface{}) error {
	parsedValues := viewModel.(mockViewModel)
	if len(parsedValues.prefix) == 0 {
		return errors.New("prefix is empty")
	}
	if len(parsedValues.text) == 0 {
		return errors.New("text is empty")
	}
	formattedString := fmt.Sprintf(tpl.compiledTpl, parsedValues.prefix, parsedValues.text)
	writer.Write([]byte(formattedString))

	return nil
}
