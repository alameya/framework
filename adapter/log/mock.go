package log

type MockLog struct {
	Provider
}

func NewMockLogger() Provider {
	return new(MockLog)
}

func (provider *MockLog) Log(message string, logLevel string) {}