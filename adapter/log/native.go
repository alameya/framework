package log

import (
	"fmt"
)

type NativeLog struct {
	Provider
}

func (provider *NativeLog) Log(message string, logLevel string) {
	println(fmt.Sprintf("%s: %s", logLevel, message))
}

func NewNativeLog() Provider {
	return &NativeLog{}
}