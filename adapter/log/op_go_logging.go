package log

import (
	"github.com/op/go-logging"
	"os"
)

var format = logging.MustStringFormatter(
	`%{color}%{time:01.02.06 15:04:05.000} %{shortfunc} ▶ %{level:.8s} %{id:03x}%{color:reset} %{message}`,
)

type OpLog struct {
	instance *logging.Logger
	Provider
}

func (provider *OpLog) Log(message string, logLevel string) {
	switch logLevel {
	case Debug:
		provider.instance.Debug(message)
	case Info:
		provider.instance.Info(message)
	case Notice:
		provider.instance.Notice(message)
	case Warning:
		provider.instance.Warning(message)
	case Error:
		provider.instance.Error(message)
	case Critical:
		provider.instance.Critical(message)
	}
}

func NewOpLog() Provider {
	var instance = logging.MustGetLogger("log_nix")
	backend := logging.NewLogBackend(os.Stderr, "", 0)
	backendFormatter := logging.NewBackendFormatter(backend, format)
	logging.SetBackend(backendFormatter)

	return &OpLog{
		instance: instance,
	}
}
