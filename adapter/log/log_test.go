package log

import "testing"

func TestLog_AddProviders(t *testing.T) {
	message := "test"
	logger := new(Log)
	logger.AddProviders(NewMockLogger)
	logger.Debug(message)
	logger.Info(message)
	logger.Warning(message)
	logger.Notice(message)
	logger.Error(message)
	logger.Critical(message)
	logger.Log(message, Debug)
}


