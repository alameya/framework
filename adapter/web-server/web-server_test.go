package web_server

import (
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/alameya/framework/adapter/config"
	"gitlab.com/alameya/framework/adapter/log"
	"net/http"
	"os"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestNewServer(t *testing.T) {
	server := NewServer(
		new(config.Config),
		new(http.Handler),
		new(log.Log),
	)

	Convey("test instance", t, func() {
		So(server, ShouldHaveSameTypeAs, new(WebServer))
	})
}

func TestBuildServerObj(t *testing.T) {
	var handler http.Handler

	buildServerObj("localhost", "9998", handler)
}

func TestOnShutdown(t *testing.T) {
	var a = 0
	osChannel := make(chan os.Signal, 1)
	callback := func() { a++ }
	go func() {
		time.Sleep(time.Millisecond * 200)
		osChannel <- os.Interrupt
	}()

	bindOnShutdown(&osChannel, callback)

	Convey("test on shutdown", t, func() {
		So(a, ShouldEqual, 1)
	})
}

func TestStartRunners(t *testing.T) {
	var wg sync.WaitGroup
	var testValue uint64
	testFunc := func() {
		atomic.AddUint64(&testValue, 1)
		wg.Done()
	}
	wg.Add(4)
	startRunners(testFunc, testFunc, testFunc, testFunc)

	wg.Wait()

	Convey("test runners start", t, func() {
		So(testValue, ShouldEqual, 4)
	})
}

func TestBuildTlsServerObj(t *testing.T) {
	var httpTlsServer *http.Server
	var handler http.Handler
	var host, sslPort, cacheDir, fqdn, sslEmail string
	httpTlsServer, handler = buildTlsServerObj(host, sslPort, cacheDir, fqdn, sslEmail, handler)

	Convey("test httpTlsServer", t, func() {
		So(httpTlsServer, ShouldNotBeNil)
		So(handler, ShouldNotBeNil)
	})
}
