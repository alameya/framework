package fts

import (
	"database/sql"
	"time"
)

func NewSphinxClient(credentials string) (sphinx *sql.DB) {
	sphinx, err := sql.Open("mysql",credentials)
	if err != nil {
		panic("failed to connect sphinx")
	}

	sphinx.SetMaxIdleConns(20)
	sphinx.SetMaxOpenConns(50)
	sphinx.SetConnMaxLifetime(time.Second * 10)

	return
}