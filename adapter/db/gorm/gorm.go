package gorm

import (
	"github.com/jinzhu/gorm"
)

type gormConnectionFunc func(string, ...interface{}) (*gorm.DB, error)
