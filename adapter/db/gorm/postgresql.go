package gorm

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/alameya/framework/adapter/config"
)

const (
	gormPostgresqlConnectionPattern   = "host=%s port=%s user=%s password=%s dbname=%s sslmode=disable"
	gormPostgresqlDriver              = "postgres"
	gormPostgresqlTableOptionsPattern = "CHARSET=%s"
)

func NewGORMPostgresqlConnection(config config.Configurator) *gorm.DB {
	subConfigObj := config.Sub("db")
	connectionString := gormPostgresqlBuildConnectionString(
		subConfigObj.GetString("user"),
		subConfigObj.GetString("password"),
		subConfigObj.GetString("host"),
		subConfigObj.GetString("port"),
		subConfigObj.GetString("database"),
	)

	tableOptions := gormPostgresqlBuildTableOptions(subConfigObj.GetString("charset"))

	connection := gormPostgresqlConnect(
		gorm.Open,
		connectionString, tableOptions,
		subConfigObj.GetInt("max_idle_conns"),
		subConfigObj.GetInt("max_open_conns"),
	)

	connection.Exec("SET SEARCH_PATH to " + subConfigObj.GetString("schema"))

	return connection
}

func gormPostgresqlConnect(
	connect gormConnectionFunc,
	connectionString, tableOptions string,
	maxIdleConnections, maxOpenConnections int,
) (*gorm.DB) {

	var db *gorm.DB
	var err error

	db, err = connect(gormPostgresqlDriver, connectionString)
	if err != nil {
		panic(err)
	}

	db.Set("gorm:table_options", tableOptions)
	db.DB().SetMaxIdleConns(maxIdleConnections)
	db.DB().SetMaxOpenConns(maxOpenConnections)

	return db
}

func gormPostgresqlBuildConnectionString(host, port, user, password, database string) string {
	return fmt.Sprintf(
		gormPostgresqlConnectionPattern,
		user,
		password,
		host,
		port,
		database,
	)
}

func gormPostgresqlBuildTableOptions(charset string) string {
	return fmt.Sprintf(gormPostgresqlTableOptionsPattern, charset)
}
