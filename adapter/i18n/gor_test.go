package i18n

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

type gorTestCase struct {
	key         string
	args        []string
	expectation string
}

func TestGor(t *testing.T) {
	const locale = "en-EN"
	testTable := []gorTestCase{
		{
			key:         "title",
			args:        []string{},
			expectation: "test title",
		},
		{
			key: "tpl_string",
			args: []string{"some text"},
			expectation: "tpl some text",
		},
	}

	translater := NewGor("./testdata", locale)

	Convey("Test gor translate", t, func() {
		for _, testCase := range testTable {
			result := translater.Translate(testCase.key, testCase.args...)
			So(result, ShouldEqual, testCase.expectation)
		}
	})
}
