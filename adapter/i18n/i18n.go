package i18n

const (
	RuLocale = "ru"
	DeLocale = "de"
	ItLocale = "it"
	EnLocale = "en"
	FrLocale = "fr"
)

type Translator interface {
	Translate(key string, args ...string) string
}
