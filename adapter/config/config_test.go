package config

import "testing"

const testConfigYamlFileName = "config.example.yml"


type MockConfig struct {}

func (config *MockConfig) GetString(key string) string {
	return key
}

func (config *MockConfig) GetInt(key string) int {
	return 1
}

func (config *MockConfig) Sub(key string) ProviderInterface {
	return new(MockConfig)
}


func (config *MockConfig) IsSet(key string) bool {
	return true
}

func newMockConfig() *MockConfig {
	return new(MockConfig)
}
func TestNewConfig(t *testing.T) {
	func() Configurator {
		return newMockConfig()
	}()
}
