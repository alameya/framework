package config

import (
	uberConfig "go.uber.org/config"
	"strconv"
)

type uberConfigProvider struct {
	ProviderInterface
	concrete *uberConfig.YAML
	subKey string
}

// NewUberConfig create constructor with default proxy for uberConfigProvider
func NewUberConfig(fileNames ...string)  Configurator {
	var proxyObj Config
	var config uberConfigProvider
	var options []uberConfig.YAMLOption

	for _, filename := range fileNames{
		option := uberConfig.File(filename)
		options = append(options, option)
	}

	yaml, err := uberConfig.NewYAML(options...)
	if err != nil {
		panic(err)
	}

	config.concrete = yaml

	return proxyObj.setProvider(&config)
}

func (config *uberConfigProvider) GetString(key string) string {
	fullKey := config.uberConfigBuiltKey(key)

	return config.concrete.Get(fullKey).String()
}

func (config *uberConfigProvider) GetInt(key string) int {
	str := config.GetString(key)
	integer, err := strconv.Atoi(str)

	if err != nil {
		panic(err)
	}

	return integer
}

func (config *uberConfigProvider) Sub(subKey string) ProviderInterface {
	subConfig := &uberConfigProvider{
		concrete: config.concrete,
		subKey: config.subKey,
	}

	if subConfig.subKey != "" {
		subConfig.subKey += "." + subKey
	} else {
		subConfig.subKey = subKey
	}

	return subConfig
}

func (config *uberConfigProvider) IsSet(key string) bool {
	fullKey := config.uberConfigBuiltKey(key)

	return config.concrete.Get(fullKey).HasValue()
}

func (config *uberConfigProvider) uberConfigBuiltKey(key string) string {
	if config.subKey != "" {
		return config.subKey + "." + key
	}

	return key
}
