package cache

import (
	"errors"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

const FileKeyMinLength = 5
const FileKeyMinLengthError = "key length is less than minimum"

type File struct{
	Cache
	dir string
	expiration int64
}

func NewFileCache(dir string, expiration int64) (*File, error) {
	if dir == "" {
		dir = os.TempDir() + "cache/"
	}

	if !strings.HasSuffix(dir, "/") {
		dir += "/"
	}

	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return nil, err
	}

	return &File{
		dir: dir,
		expiration: expiration,
	}, nil
}

func (file *File) Get(hashKey string) (value []byte, err error) {
	path, err := buildFilePath(file.dir, hashKey)
	if err != nil {
		return nil, err
	}

	info, err := os.Stat(path)
	if err != nil {
		return nil, nil
	}

	isExpired := info.ModTime().Unix() + file.expiration < time.Now().Unix()
	if isExpired {
		if err == nil {
			return nil, file.Delete(hashKey)
		}

		return nil, nil
	}

	return ioutil.ReadFile(path)
}


func (file *File) Set(hashKey string, value []byte) (err error) {
	path, err := buildFilePath(file.dir, hashKey)
	if err != nil {
		return err
	}

	if err = os.MkdirAll(file.dir + hashKey[:2] + "/" + hashKey[2:4], os.ModePerm); err != nil {
		return err
	}

	if err = ioutil.WriteFile(path, value, 0664); err != nil {
		return err
	}

	return
}

func (file *File) Delete(hashKey string) error {
	filePath, err := buildFilePath(file.dir, hashKey)
	if err != nil {
		return err
	}

	if err = os.Remove(filePath); err!= nil {
		return err
	}

	// remove directory if empty. ignore error
	switch {
	case os.Remove(file.dir + hashKey[:2] + "/" + hashKey[2:4]) != nil:
		break
	case os.Remove(file.dir + hashKey[:2]) != nil:
		break
	case os.Remove(file.dir) != nil:
		break
	}

	return nil
}

func buildFilePath(dir, hashKey string) (string, error) {
	dirPath, err := buildDirPath(dir, hashKey)
	if err != nil {
		return "", err
	}

	filePath := dirPath + hashKey[4:]

	return filePath, nil
}

func buildDirPath(dir, hashKey string) (string, error) {
	if len(hashKey) < FileKeyMinLength {
		return "", errors.New(FileKeyMinLengthError)
	}

	return dir + hashKey[:2] + "/" + hashKey[2:4] + "/", nil
}