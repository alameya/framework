/*
paginator package

// full example |<=|1|...|5|6|7|8|9|...|234|=>|
// 0. if isShowPrevNext -> |<=|...|=>|
// 1. if isShowMinMax -> 1
// 2. -> ...
// 3. countAround - -> 5|6
// 4. current -> 7
// 5. countAroundCursor + -> 8|9
// 6. -> ...
// 7. if isShowMinMax -> |234|
 */
package paginator

import (
	"errors"
)

// Paginator - Helper for page pagination
type Paginator struct {
	total           uint
	pageNo          uint
	countPerPage    uint
	cursorIteration uint
	// countAroundCurrent values explanation
	// -1 => don't show even current page |prev|next|
	// 0 => show current number only |1|...|7|...|234|
	// > 1 (2) => |1|...|5|6|7|8|9|...|234|
	countAroundCurrent int
	isShowPrevNext     bool
	isShowMinMax       bool
	elements           []Cursor
}

type Cursor struct {
	IsCurrent,
	IsFirst,
	IsPrevious,
	IsShowValue,
	IsNext,
	HasAction,
	IsLast bool
	Value uint
}

const (
	// CountPerPageDefault - Count per page. You can use this for default value.
	CountPerPageDefault = 10
	// PageNoDefault - Page number by default
	PageNoDefault = 1
	// ErrorMsgPageNoLT1 - Error message when page number < 1
	ErrorMsgPageNoLT1 = "pageNo < 1"
	// ErrorMsgPageNoGTMaxPageNo - Error message when page number > maximum
	ErrorMsgPageNoGTMaxPageNo = "pageNo > MaxPageNo"
	minPageNo                 = 1
)

// NewPaginator - Paginator constructor
func NewPaginator(
	pageNo, countPerPage, total uint,
	countAroundCurrent int,
	isShowPrevNext, isShowMinMax bool,
) (Paginator, error) {
	paginatorObj := Paginator{
		countPerPage:       countPerPage,
		total:              total,
		pageNo:             pageNo,
		cursorIteration:    0,
		countAroundCurrent: countAroundCurrent,
		isShowPrevNext:     isShowPrevNext,
		isShowMinMax:       isShowMinMax,
	}

	if pageNo < 1 {
		return paginatorObj, errors.New(ErrorMsgPageNoLT1)
	}

	if pageNo > paginatorObj.GetMaxPageNo() {
		return paginatorObj, errors.New(ErrorMsgPageNoGTMaxPageNo)
	}

	paginatorObj.preSetElements()

	return paginatorObj, nil
}

// GetPageNo - return page number
func (paginator *Paginator) GetPageNo() uint {
	return paginator.pageNo
}

// GetTotal - return total pages
func (paginator *Paginator) GetTotal() uint {
	return paginator.total
}

// SetTotal - set total pages
func (paginator *Paginator) SetTotal(total uint) {
	paginator.total = total
	paginator.preSetElements()
	return
}

// GetLimit - count per page showed or limit (for database)
func (paginator *Paginator) GetLimit() uint {
	return paginator.countPerPage
}

// GetOffset - db offset
func (paginator *Paginator) GetOffset() uint {
	return (paginator.pageNo - 1) * paginator.countPerPage
}

// IsFirst - is it first page?
func (paginator *Paginator) IsFirst() bool {
	return paginator.pageNo == minPageNo
}

// IsLast - is it last page?
func (paginator *Paginator) IsLast() bool {
	return paginator.pageNo*paginator.countPerPage >= paginator.total
}

// GetMaxPageNo - Maximum page number
func (paginator *Paginator) GetMaxPageNo() uint {
	if paginator.total%paginator.countPerPage > 0 {
		return paginator.total/paginator.countPerPage + 1
	}

	return paginator.total / paginator.countPerPage
}

func (paginator *Paginator) getMinAround() uint {
	var countAroundCurrentUint uint
	if paginator.countAroundCurrent < 0 {
		countAroundCurrentUint = 0
	} else {
		countAroundCurrentUint = uint(paginator.countAroundCurrent)
	}

	if paginator.pageNo < countAroundCurrentUint {
		return minPageNo
	}

	value := paginator.pageNo - countAroundCurrentUint

	if value < minPageNo {
		return minPageNo
	}

	return value
}

func (paginator *Paginator) getMaxAround() uint {
	value := paginator.pageNo + uint(paginator.countAroundCurrent)
	maxValue := paginator.GetMaxPageNo()
	if value > maxValue {
		return maxValue
	}

	return value
}

func (paginator *Paginator) preSetElements() {
	maxPageNo := paginator.GetMaxPageNo()
	minAround := paginator.getMinAround()
	maxAround := paginator.getMaxAround()


	// previous next elements
	if paginator.isShowPrevNext {
		if paginator.pageNo != minPageNo && minAround > minPageNo {
			paginator.elements = append(paginator.elements, Cursor{
				IsPrevious: true,
				Value:      paginator.pageNo - 1,
				HasAction:  true,
			})
		}

		if paginator.pageNo != maxPageNo && maxPageNo > maxAround  {
			defer func() {
				paginator.elements = append(paginator.elements, Cursor{
					IsNext:    true,
					Value:     paginator.pageNo + 1,
					HasAction: true,
				})
			}()
		}
	}

	// first element
	if paginator.isShowMinMax {
		if minAround > minPageNo {
			paginator.elements = append(paginator.elements, Cursor{
				IsFirst:     true,
				IsShowValue: true,
				Value:       minPageNo,
				HasAction:   true,
			})
		}

		// here is one page only
		if maxPageNo == minPageNo {
			return
		}

		// last element
		if maxAround < maxPageNo {
			defer func() {
				paginator.elements = append(paginator.elements, Cursor{
					IsLast:      true,
					IsShowValue: true,
					Value:       maxPageNo,
					HasAction:   true,
				})
			}()
		}
	}

	if paginator.countAroundCurrent < 0 {
		return
	}

	// dots
	if paginator.countAroundCurrent > 0 {
		if minAround > minPageNo + 1 {
			paginator.elements = append(paginator.elements, Cursor{})
		}
		if maxAround + 1 < maxPageNo {
			defer func() {
				paginator.elements = append(paginator.elements, Cursor{})
			}()
		}
	}

	// elements around and current
	for i := minAround; i != maxAround+1; i++ {
		paginator.elements = append(paginator.elements, Cursor{
			IsFirst:     i == 1,
			IsLast:      i == maxPageNo,
			IsShowValue: true,
			Value:       i,
			HasAction:   paginator.pageNo != i,
		})
	}

	return
}

// Next - iteration
func (paginator *Paginator) Next() *Cursor {

	elementsCount := len(paginator.elements)
	if paginator.cursorIteration == uint(elementsCount) {
		return nil
	}

	cursor := paginator.elements[paginator.cursorIteration]
	paginator.cursorIteration++

	cursor.IsCurrent = paginator.pageNo == cursor.Value

	return &cursor
}
