module gitlab.com/alameya/framework

require (
	github.com/CloudyKit/fastprinter v0.0.0-20170127035650-74b38d55f37a // indirect
	github.com/CloudyKit/jet v2.1.2+incompatible
	github.com/andybalholm/cascadia v1.0.0 // indirect
	github.com/asaskevich/govalidator v0.0.0-20180720115003-f9ffefc3facf // indirect
	github.com/aws/aws-sdk-go v1.23.13 // indirect
	github.com/disintegration/imaging v1.6.1 // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/gorilla/sessions v1.1.3 // indirect
	github.com/gosimple/slug v1.5.0 // indirect
	github.com/jinzhu/configor v1.1.1 // indirect
	github.com/jinzhu/gorm v1.9.4
	github.com/jinzhu/now v1.0.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/microcosm-cc/bluemonday v1.0.2 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/qor/admin v0.0.0-20190329022438-b2f472167d02 // indirect
	github.com/qor/assetfs v0.0.0-20170713023933-ff57fdc13a14 // indirect
	github.com/qor/cache v0.0.0-20171031031927-c9d48d1f13ba // indirect
	github.com/qor/i18n v0.0.0-20181014061908-f7206d223bcd
	github.com/qor/media v0.0.0-20190830035427-8c0c3e14880b // indirect
	github.com/qor/middlewares v0.0.0-20170822143614-781378b69454 // indirect
	github.com/qor/oss v0.0.0-20190603071501-90a5bbaee07c // indirect
	github.com/qor/qor v0.0.0-20190319081902-186b0237364b // indirect
	github.com/qor/responder v0.0.0-20171031032654-b6def473574f // indirect
	github.com/qor/roles v0.0.0-20171127035124-d6375609fe3e // indirect
	github.com/qor/serializable_meta v0.0.0-20180510060738-5fd8542db417 // indirect
	github.com/qor/session v0.0.0-20170907035918-8206b0adab70 // indirect
	github.com/qor/validations v0.0.0-20171228122639-f364bca61b46 // indirect
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
	github.com/stretchr/testify v1.3.0
	github.com/theplant/cldr v0.0.0-20170713054817-b97a4c2a76cf // indirect
	github.com/theplant/htmltestingutils v0.0.0-20190423050759-0e06de7b6967 // indirect
	github.com/theplant/testingutils v0.0.0-20190603093022-26d8b4d95c61 // indirect
	github.com/yosssi/gohtml v0.0.0-20190128141317-9b7db94d32d9 // indirect
	gitlab.com/rzpublic/translit v2.0.0+incompatible
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/config v1.3.1
	go.uber.org/multierr v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472
	golang.org/x/net v0.0.0-20190827160401-ba9fcec4b297 // indirect
	golang.org/x/sys v0.0.0-20190830142957-1e83adbbebd0 // indirect
	golang.org/x/text v0.3.2 // indirect
	pkg.re/check.v1 v0.0.0-20180628173108-788fd7840127 // indirect
)
